<?php

/**
 * @file
 * Contains dismissible_message_bar.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function dismissible_message_bar_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the dismissible_message_bar module.
    case 'help.page.dismissible_message_bar':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Custom notification functionality for Drupal') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function dismissible_message_bar_theme() {
  $theme['dmb_notification'] = [
    'render element' => 'elements',
    'file' => 'dmb_notifications_entity.page.inc',
    'template' => 'dmb_notification',
  ];
  return $theme;
}

/**
 * Implements hook_theme_suggestions_HOOK().
 *
 * Add template suggestions for notifications, based on ID, view mode and
 * bundle.
 */
function dismissible_message_bar_theme_suggestions_dmb_notifications_entity(array $variables) {
  $suggestions = [];
  $notification = $variables['elements']['#dmb_notifications_entity'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'dmb_notifications_entity__' . $sanitized_view_mode;

  if (!empty($notification->bundle())) {
    $suggestions[] = 'dmb_notifications_entity__' . $notification->bundle();
    $suggestions[] = 'dmb_notifications_entity__' . $notification->bundle() . '__' . $sanitized_view_mode;
  }

  $suggestions[] = 'dmb_notifications_entity__' . $notification->id();
  $suggestions[] = 'dmb_notifications_entity__' . $notification->id() . '__' . $sanitized_view_mode;

  return $suggestions;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function dismissible_message_bar_preprocess_html(&$variables) {
  /** @var \Drupal\dismissible_message_bar\Service\DmbNotificationService $dmb_manager */
  $dmb_manager = Drupal::service('dismissible_message_bar.notification_service');
  if ($dmb_manager->currentPageHasNotifications()) {
    $variables['attributes']['class'][] = 'dmb-notification';
  }
}
