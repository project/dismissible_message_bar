CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Overview
  * Configuration
  * Maintainers


INTRODUCTION
------------

The Dismissible Message Bar module allows you to easily create dismissible
messages as blocks with embedded Paragraph components on your site. These
dismissible messages can be placed in any block region, can be set to display
only on certain paths (including wildcard paths), and can be set to expire on
a specified date. Additionally, once a user clicks the close button on a message,
a cookie is set so that the message does not continue to pop up once acknowledged
by a user.

  * For a full description of the module, visit the project page:
    https://www.drupal.org/project/dismissible_message_bar

  * To submit bug reports and feature suggestions, or to track changes:
    https://www.drupal.org/project/issues/dismissible_message_bar


REQUIREMENTS
------------

This module requires the following modules:

  * Paragraphs (https://www.drupal.org/project/paragraphs)


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module.

  * Visit:
    https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
    for further information.


OVERVIEW
--------

  * Upon installation, this module will create a new entity type called
    "DMB Notification", and will create a default DMB Notification bundle
    called "Default". The Default bundle includes the following fields:

  * Content: This field allows you to add any Paragraph component type that
    exists on your site. Be sure to edit this field and allow any Paragraphs
    types you would like to add to your message.

  * Content Types: This field allows you restrict messages to only certain
    content types.

  * Date Range: This field allows you to set the dates for which the message
    will display.

  * Notification Pages: This field allows you to limit the message to only
    certain paths (including wildcard paths, such as /user/*

  * Sitewide: This field allows you to display the message for all pages,
    ignoring the location restriction.


CONFIGURATION
-------------

  * To begin adding dismissible messages to your site, navigation to
    /admin/structure/block and place the "DMB Notifications block" in a
    region of your choosing.

  * Navigate to Content > DMB Notifications > Add a new DMB Notification
    (/dmb_notifications/add) and choose a type of notification to add. Note
    that this module provides a Default bundle by default.

  * Once you've chosen your type, give it a name, some content (using the
    Paragraphs module), and set some content types, dates, and pages for
    display.

  * You should now see the notification showing up in the specified region
    according to the specified criteria. Note that if you click the close
    button, a cookie is set and the message will not reappear until cookies
    are cleared.


MAINTAINERS
-----------

  * Joe Flores (Mojiferous) - https://www.drupal.org/u/mojiferous

This project has been sponsored by:

  * Elevated Third

Specializing in Drupal-powered digital experiences that get results. Visit
https://www.elevatedthird.com
