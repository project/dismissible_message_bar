<?php

namespace Drupal\dismissible_message_bar\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Notifications entity entities.
 *
 * @ingroup dismissible_message_bar
 */
interface DmbNotificationsEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Notifications entity name.
   *
   * @return string
   *   Name of the Notifications entity.
   */
  public function getName();

  /**
   * Sets the Notifications entity name.
   *
   * @param string $name
   *   The Notifications entity name.
   *
   * @return \Drupal\dismissible_message_bar\Entity\DmbNotificationsEntityInterface
   *   The called Notifications entity entity.
   */
  public function setName($name);

  /**
   * Gets the Notifications entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Notifications entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Notifications entity creation timestamp.
   *
   * @param int $timestamp
   *   The Notifications entity creation timestamp.
   *
   * @return \Drupal\dismissible_message_bar\Entity\DmbNotificationsEntityInterface
   *   The called Notifications entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Notifications entity published status indicator.
   *
   * Unpublished Notifications entity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Notifications entity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Notifications entity.
   *
   * @param bool $published
   *   TRUE to set this Notifications entity to published, FALSE to set it to
   *   unpublished.
   *
   * @return \Drupal\dismissible_message_bar\Entity\DmbNotificationsEntityInterface
   *   The called Notifications entity entity.
   */
  public function setPublished($published);

}
