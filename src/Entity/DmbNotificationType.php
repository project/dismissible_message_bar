<?php

namespace Drupal\dismissible_message_bar\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the Notification Type configuration entity.
 *
 * @ingroup dismissible_message_bar
 *
 * @ConfigEntityType(
 *   id = "dmb_notification_type",
 *   label = @Translation("DMB Notification Type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\dismissible_message_bar\Form\DmbNotificationTypeForm",
 *       "edit" = "Drupal\dismissible_message_bar\Form\DmbNotificationTypeForm",
 *       "delete" = "Drupal\dismissible_message_bar\Form\DmbNotificationTypeDeleteConfirm"
 *     },
 *     "list_builder" = "Drupal\dismissible_message_bar\Controller\DmbNotificationTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer dmb notifications entities",
 *   config_prefix = "type",
 *   bundle_of = "dmb_notifications_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/dmb_notification_types/add",
 *     "edit-form" = "/admin/structure/dmb_notification_types/manage/{dmb_notification_type}",
 *     "delete-form" = "/admin/structure/dmb_notification_types/manage/{dmb_notification_type}/delete",
 *     "collection" = "/admin/structure/dmb_notification_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class DmbNotificationType extends ConfigEntityBundleBase implements ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * The machine name of the Notification Type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Notification Type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of the Notification Type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
