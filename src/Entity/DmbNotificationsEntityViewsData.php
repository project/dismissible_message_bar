<?php

namespace Drupal\dismissible_message_bar\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for DMB Notifications entity entities.
 */
class DmbNotificationsEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
