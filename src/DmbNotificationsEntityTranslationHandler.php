<?php

namespace Drupal\dismissible_message_bar;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for dmb_notifications_entity.
 */
class DmbNotificationsEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
