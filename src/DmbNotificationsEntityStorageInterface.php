<?php

namespace Drupal\dismissible_message_bar;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the storage handler class for DMB Notifications entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Notifications entity entities.
 *
 * @ingroup dismissible_message_bar
 */
interface DmbNotificationsEntityStorageInterface extends ContentEntityStorageInterface {


}
