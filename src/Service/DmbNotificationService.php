<?php

namespace Drupal\dismissible_message_bar\Service;

use DateTimeZone;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DmbNotificationService.
 *
 * @package Drupal\dismissible_message_bar\Service
 */
class DmbNotificationService {

  use StringTranslationTrait;

  /**
   * Current path stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected CurrentPathStack $currentPathStack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * Language Manager interface.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected PathMatcherInterface $pathMatcher;

  /**
   * An alias manager to find the alias for the current system path.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected AliasManagerInterface $aliasManager;

  /**
   * Constructs a new NotificationsService object.
   *
   * @param \Drupal\Core\Path\CurrentPathStack $current_path_stack
   *   Current path stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Stack.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language manager service.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Path matcher service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   Alias manager service.
   */
  public function __construct(
    CurrentPathStack $current_path_stack,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack,
    Renderer $renderer,
    LanguageManagerInterface $language_manager,
    PathMatcherInterface $path_matcher,
    AliasManagerInterface $alias_manager) {

    $this->currentPathStack = $current_path_stack;
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->languageManager = $language_manager;
    $this->pathMatcher = $path_matcher;
    $this->aliasManager = $alias_manager;
  }

  /**
   * Return the number of seconds to the next notification change.
   *
   * @return int
   *   Seconds until the next notification change.
   */
  public function returnNextChangeSecondsFromNow():int {
    // Get the current time.
    $gmtTimezone = new DateTimeZone('GMT');

    $now_time = new DrupalDateTime('now', $gmtTimezone);
    $now = $now_time->getTimestamp();

    // Least time will be our cache time if no notifications
    // expire of begin after now.
    $least_time = 0;
    // Pass -1 (PERMANENT) as a default.
    $result = -1;

    try {
      $storage = $this->entityTypeManager->getStorage('dmb_notifications_entity');
      $query = $storage->getQuery()
        ->accessCheck();

      $notifications = $query->execute();
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      // Unable to load the notifications, quietly die.
      $notifications = NULL;
    }

    if (!empty($notifications) && isset($storage)) {
      $notifications = $storage->loadMultiple($notifications);

      foreach ($notifications as $notification) {

        // If the notification does not have a date range, set the start to be
        // 1 day ago and the end 1 day from now.
        $date_range = $notification->get('field_notification_date_range')
          ->first();

        if (!empty($date_range->value)) {
          $start_date = new DrupalDateTime($date_range->value, $gmtTimezone);
          $date_range_begin = $start_date->getTimestamp();

          if ($date_range_begin > $now) {
            // This notification begins after now.
            $diff_time = $date_range_begin - $now;
            if (($diff_time < $least_time) || empty($least_time)) {
              // Least time has not been set or this is less than it.
              $least_time = $diff_time;
            }
          }
        }

        if (!empty($date_range->end_value)) {
          $end_date = new DrupalDateTime($date_range->end_value, $gmtTimezone);
          $date_range_end = $end_date->getTimestamp();

          if ($date_range_end > $now) {
            // This notification ends after now.
            $diff_time = $date_range_end - $now;
            if (($diff_time < $least_time) || empty($least_time)) {
              // Least time has not been set or this is less than it.
              $least_time = $diff_time;
            }
          }
        }
      }
    }

    if (!empty($least_time)) {
      $result = $least_time;
    }

    return $result;

  }

  /**
   * Returns array of all notifications.
   *
   * @return array
   *   Notification values to insert into drupal settings.
   */
  public function returnAllNotifications():array {
    $result = [];

    try {
      $storage = $this->entityTypeManager->getStorage('dmb_notifications_entity');
      $query = $storage->getQuery()
        ->sort('created', 'DESC')
        ->accessCheck(TRUE);

      $notifications = $query->execute();
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      // Unable to load the notifications, quietly die.
      $notifications = NULL;
    }

    if (!empty($notifications) && isset($storage)) {
      $notifications = $storage->loadMultiple($notifications);

      foreach ($notifications as $notification) {
        // We're letting JS decide the date/time of the notification.
        // By default we display all notifications, and only remove
        // notifications from more than 24 hours ago.
        // OR notifications that do not start for more than 24 hours.
        $display = TRUE;

        // If the notification does not have a date range, set the start to be
        // 1 day ago and the end 1 day from now.
        $date_range = FALSE;
        if ($notification->hasField('field_notification_date_range') && !$notification->get('field_notification_date_range')->isEmpty()) {
          $date_range = $notification->get('field_notification_date_range')->first();
        }

        $gmtTimezone = new DateTimeZone('GMT');

        $now_time = new DrupalDateTime('now', $gmtTimezone);
        $now = $now_time->getTimestamp();

        if (!empty($date_range) && !empty($date_range->value)) {
          $start_date = new DrupalDateTime($date_range->value, $gmtTimezone);
          $date_range_begin = $start_date->getTimestamp();

          // Filter notifications that start more than a day from now.
          if (($date_range_begin - $now) > 86400) {
            $display = FALSE;
          }
        }

        if (!empty($date_range) && !empty($date_range->end_value)) {
          $end_date = new DrupalDateTime($date_range->end_value, $gmtTimezone);
          $date_range_end = $end_date->getTimestamp();

          // Filter notifications that end more than a day ago.
          // This range should prevent most timezone-related mess.
          if (($now - $date_range_end) > 86400) {
            $display = FALSE;
          }
        }

        if ($display) {
          // Build the view of referenced content.
          if (!isset($notification->field_p_content)) {
            // This notification does not have content, ignore it.
            continue;
          }

          $content = $notification->field_p_content;
          $viewBuilder = $this->entityTypeManager->getViewBuilder('paragraph');
          $content = $viewBuilder->viewField($content, ['label' => 'hidden']);

          $notification_id = $notification->id();

          // Automatically dismiss bar after set time.
          $auto_dismiss = FALSE;
          $dismiss_time = FALSE;
          if ($notification->hasField('field_auto_dismiss') && !$notification->get('field_auto_dismiss')->isEmpty()) {
            $auto_dismiss = $notification->get('field_auto_dismiss')->value;
            if ($notification->hasField('field_dismiss_time') && !$notification->get('field_dismiss_time')->isEmpty()) {
              $dismiss_time = $notification->get('field_dismiss_time')->value;
            }
          }

          // Get the cookie expiration information.
          $cookie_expiration = 365;
          if ($notification->hasField('field_cookie_expiration') && !$notification->get('field_cookie_expiration')->isEmpty()) {
            $cookie_expiration = $notification->get('field_cookie_expiration')->value;
          }

          // Set the notification's classes.
          $classes = [
            'dmb-notification',
          ];
          // If notification has cookie turned off, set class.
          if ($notification->hasField('field_cookie_off') && !$notification->get('field_cookie_off')->isEmpty() && !empty($notification->get('field_cookie_off')->value)) {
            $classes[] = 'cookie-off';
          }
          // Get the notification type (from term reference), set class.
          if ($notification->hasField('field_notification_type') && !$notification->get('field_notification_type')->isEmpty()) {
            $classes[] = strtolower(str_replace(' ', '-', $notification->get('field_notification_type')->entity->name->value));
          }

          $rendered_content = [
            '#type' => 'container',
            '#attributes' => [
              'class' => $classes,
              'dmb-notification-id' => $notification_id,
              'dmb-cookie-expiration' => $cookie_expiration ?? 365,
              'dmb-auto-dismiss' => $auto_dismiss ?? FALSE,
              'dmb-dismiss-time' => $dismiss_time ?? 0,
            ],
            'notification' => [
              '#type' => 'container',
              '#attributes' => [
                'class' => ["dmb-notification-content"],
              ],
              'content' => $content,
            ],
            'close_notification' => [
              '#title' => $this->t('Close notification'),
              '#type' => 'link',
              '#url' => Url::fromUserInput('#'),
              '#attributes' => [
                'class' => ["close-dmb-notification"],
              ],
            ],
          ];
          $rendered_content = $this->renderer->renderPlain($rendered_content);

          // Get allowed content types.
          $allowed_content_types = [];
          if ($notification->hasField('field_content_types') && !$notification->get('field_content_types')->isEmpty()) {
            foreach ($notification->get('field_content_types') as $content_type_ref) {
              $allowed_content_types[] = $content_type_ref->target_id;
            }
          }

          // Get sitewide notifications.
          $sitewide = '';
          if ($notification->hasField('field_sitewide') && !$notification->get('field_sitewide')->isEmpty()) {
            $sitewide = $notification->get('field_sitewide')->value;
          }

          // Get allowed pages.
          $allowed_pages = [];
          if ($notification->hasField('field_notification_pages') && !$notification->get('field_notification_pages')->isEmpty()) {
            $allowed_pages_raw = $notification->get('field_notification_pages')->value;

            $allowed_pages_raw = explode(PHP_EOL, $allowed_pages_raw);
            foreach ($allowed_pages_raw as $allowed_page) {
              $allowed_page = trim($allowed_page);
              if (!empty($allowed_page)) {
                // Trim off trailing slashes unless this is the front page.
                $allowed_page = $allowed_page === '/' ? $allowed_page : rtrim($allowed_page, '/');
                // Make sure the page has a leading slash.
                // JS window.location.pathname always has a leading slash.
                if (str_starts_with($allowed_page, '!') && substr($allowed_page, 1, 1) !== '/') {
                  $allowed_page = '!/' . substr($allowed_page, 1);
                }
                elseif (!str_starts_with($allowed_page, '/') && !str_starts_with($allowed_page, '!')) {
                  $allowed_page = '/' . $allowed_page;
                }
                $allowed_pages[] = $allowed_page;
              }
            }

          }

          // Get disallowed pages.
          $disallowed_pages = [];
          if ($notification->hasField('field_excluded_pages') && !$notification->get('field_excluded_pages')->isEmpty()) {
            $disallowed_pages_raw = $notification->get('field_excluded_pages')->value;

            $disallowed_pages_raw = explode(PHP_EOL, $disallowed_pages_raw);
            foreach ($disallowed_pages_raw as $disallowed_page) {
              $disallowed_page = trim($disallowed_page);
              if (!empty($disallowed_page)) {
                // Trim off trailing slashes unless this is the front page.
                $disallowed_page = $disallowed_page === '/' ? $disallowed_page : rtrim($disallowed_page, '/');
                // Make sure the page has a leading slash.
                // JS window.location.pathname always has a leading slash.
                if (str_starts_with($disallowed_page, '!') && substr($disallowed_page, 1, 1) !== '/') {
                  $disallowed_page = '!/' . substr($disallowed_page, 1);
                }
                elseif (!str_starts_with($disallowed_page, '/') && !str_starts_with($disallowed_page, '!')) {
                  $disallowed_page = '/' . $disallowed_page;
                }
                $disallowed_pages[] = $disallowed_page;
              }
            }

          }

          $start_time = (!empty($date_range) && isset($date_range->value)) ? $date_range->value . '+00:00' : '';
          $end_time = (!empty($date_range) && isset($date_range->end_value)) ? $date_range->end_value . '+00:00' : '';
          $result[$notification_id] = [
            'content' => $rendered_content,
            'startTime' => $start_time,
            'endTime' => $end_time,
            'contentTypes' => $allowed_content_types,
            'sitewide' => $sitewide,
            'pathLimit' => $allowed_pages,
            'excluded' => $disallowed_pages,
            'id' => $notification_id,
          ];

        }
      }

    }

    return $result;
  }

  /**
   * Check if current page has visible notifications.
   */
  public function currentPageHasNotifications(): bool {
    $notifications = $this->returnAllNotifications();
    if (!$notifications) {
      return FALSE;
    }
    $current_request = $this->requestStack->getCurrentRequest();
    $content_type = $this->getCurrentContentType($current_request);
    $content_path = $current_request->getPathInfo();

    foreach ($notifications as $notification) {
      $sitewide = (bool) $notification['sitewide'];
      $show_notification = $sitewide;
      // Check for content type match.
      if (!empty($notification['contentTypes']) && $content_type) {
        if (in_array($content_type, $notification['contentTypes'])) {
          $show_notification = TRUE;
        }
      }
      // Check current path match.
      if (!$sitewide && !empty($notification['pathLimit'])) {
        $path_limit_pattern = implode(PHP_EOL, $notification['pathLimit']);
        if ($this->pathMatcher->matchPath($content_path, $path_limit_pattern)) {
          $show_notification = TRUE;
        }
      }
      // Check for excluded pages.
      // This overrides sitewide as well.
      if (!empty($notification['excluded'])) {
        $path_limit_pattern = implode(PHP_EOL, $notification['excluded']);
        if ($this->pathMatcher->matchPath($content_path, $path_limit_pattern)) {
          $show_notification = FALSE;
        }
      }
      // Check if notification is disabled by user.
      $disabled_notifications = $this->getDisabledNotifications();
      if ($disabled_notifications && in_array(
          $notification['id'],
          $disabled_notifications
        )) {
        $show_notification = FALSE;
      }

      // Check dates.
      if ($show_notification) {
        $now_time = new DrupalDateTime('now', 'GMT');
        if (!empty($notification['startTime'])) {
          $notification['startTime'] = str_replace(
            '+00:00',
            '',
            $notification['startTime']
          );
          $start_time = DrupalDateTime::createFromFormat(
            DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
            $notification['startTime'],
            'GMT'
          );
          if ($start_time->getTimestamp() > $now_time->getTimestamp()) {
            $show_notification = FALSE;
          }
        }
        if (!empty($notification['endTime'])) {
          $notification['endTime'] = str_replace(
            '+00:00',
            '',
            $notification['endTime']
          );
          $end_time = DrupalDateTime::createFromFormat(
            DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
            $notification['endTime'],
            'GMT'
          );
          if ($end_time->getTimestamp() < $now_time->getTimestamp()) {
            $show_notification = FALSE;
          }
        }
      }

      if (!$show_notification) {
        continue;
      }

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Get the current content type.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current Request.
   *
   * @return string
   *   Content type name or 'none'.
   */
  public function getCurrentContentType(Request $request):string {
    // Grab the node associated with this content.
    $node = $request->attributes->get('node');
    if (empty($node) || !is_object($node)) {
      // Set the "no content type" to 'none' so we don't have empty collision.
      $content_type = FALSE;
    }
    else {
      $content_type = $node->getType();
    }

    return $content_type;
  }

  /**
   * Get disabled notifications.
   *
   * @return array
   *   Array of disabled notification IDs.
   */
  protected function getDisabledNotifications():array {
    $saved_cookie = $_COOKIE['dismissible_message_bar'] ?? '';
    return $saved_cookie ? explode('.', $saved_cookie) : [];
  }

}
