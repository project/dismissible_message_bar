<?php

namespace Drupal\dismissible_message_bar;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the DMB Notifications entity entity.
 *
 * @see \Drupal\dismissible_message_bar\Entity\DmbNotificationsEntity.
 */
class DmbNotificationsEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\dismissible_message_bar\Entity\DmbNotificationsEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished dmb notifications entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published dmb notifications entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit dmb notifications entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete dmb notifications entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add dmb notifications entities');
  }

}
