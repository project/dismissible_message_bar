<?php

namespace Drupal\dismissible_message_bar\Plugin\Field\FieldWidget;

use Drupal\Core\Database\Database;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'content_type_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "content_type_field_widget",
 *   label = @Translation("Content type field widget"),
 *   field_types = {
 *     "content_type_field_type"
 *   }
 * )
 */
class ContentTypeFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    // $elements['size'] = [
    // '#type' => 'number',
    // '#title' => t('Size of textfield'),
    // '#default_value' => $this->getSetting('size'),
    // '#required' => TRUE,
    // '#min' => 1,
    // ];
    // $elements['placeholder'] = [
    // '#type' => 'textfield',
    // '#title' => t('Placeholder'),
    // '#default_value' => $this->getSetting('placeholder'),
    // '#description' => t('Text that will be shown inside the field until a
    // value is entered. This hint is usually a sample value or a brief
    // description of the expected format.'),
    // ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items[$delta]->value ?? '';
    $value = explode(',', $value);

    $element['value'] = $element + [
      '#type' => 'checkboxes',
      '#default_value' => $value,
      '#options' => $this->getAllContentTypeNames(),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if (!empty($values[0]['value'])) {
      $new_value = [];
      foreach ($values[0]['value'] as $value) {
        if (!empty($value)) {
          $new_value[] = $value;
        }
      }
      $values[0]['value'] = implode(',', $new_value);
    }

    return $values;
  }

  /**
   * Returns an array of all possible content node types.
   *
   * @return array
   *   An array of all possible content node types.
   */
  private function getAllContentTypeNames() {
    $db_connection = Database::getConnection();

    $select = $db_connection->query('SELECT DISTINCT type FROM {node}');

    $data = $select->fetchAllAssoc('type');
    $data = array_keys($data);
    $ret_val = [];
    foreach ($data as $datum) {
      $ret_val[$datum] = $datum;
    }

    return $ret_val;
  }

}
