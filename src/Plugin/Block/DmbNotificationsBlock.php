<?php

namespace Drupal\dismissible_message_bar\Plugin\Block;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dismissible_message_bar\Service\DmbNotificationService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'DmbNotificationsBlock' block.
 *
 * @Block(
 *  id = "dmb_notifications_block",
 *  admin_label = @Translation("DMB Notifications block"),
 * )
 */
class DmbNotificationsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * @var \Drupal\dismissible_message_bar\Service\DmbNotificationService
   */
  protected $dmbService;

  /**
   * Creates a DmbNotificationsBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Drupal entityTypeManager.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\dismissible_message_bar\Service\DmbNotificationService $dmb_service
   *   Dismissible message bar service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Request $request, DmbNotificationService $dmb_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
    $this->dmbService = $dmb_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('dismissible_message_bar.notification_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $type_id = $config['taxonomy_block'] ?? '';
    $type_class = 'dmb-notifications-ajax-wrapper';
    if (!empty($type_id)) {
      $type_class .= '-' . $type_id;
    }

    // Since LazyBuilder approach does not bypass Varnish, only placeholder is
    // rendered here and populated later through an AJAX call.
    $build = [
      '#type' => 'container',
      'dmb_notifications' => [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'class' => [
            $type_class,
          ],
        ],
      ],
      '#attached' => [
        'library' => [
          'dismissible_message_bar/dismissible_message_bar',
        ],
      ],
    ];

    $dmb_settings_array = [
      'notificationWrapper' => '.' . $type_class,
      'contentType' => $this->getCurrentContentType($this->request),
      'contentPath' => $this->request->getPathInfo(),
      'notificationType' => $type_id,
    ];

    $build['#attached']['drupalSettings']['dmbNotificationEntities'] = $this->dmbService->returnAllNotifications();

    $build['#attached']['drupalSettings']['dmbNotifications'][$type_class] = $dmb_settings_array;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $term = NULL;
    if (!empty($config['taxonomy_block'])) {
      try {
        $term = $this->entityTypeManager->getStorage('taxonomy_term')
          ->load($config['taxonomy_block']);
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
        // Unable to load the taxonomy term entity type manager.
        $term = NULL;
      }
    }

    $form['taxonomy_block'] = [
      '#title' => $this->t('Notification type'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => ['dmb_notification_type'],
      ],
      '#default_value' => $term,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->setConfigurationValue('taxonomy_block', $values['taxonomy_block']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'url.path';
    $contexts[] = 'cookies:dismissible_message_bar';

    return $contexts;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['dmb_block']);
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheMaxAge() {
    return $this->dmbService->returnNextChangeSecondsFromNow();
  }

  /**
   * Get the current content type.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current Request.
   *
   * @return string
   *   Content type name or 'none'.
   */
  public function getCurrentContentType(Request $request) {
    return $this->dmbService->getCurrentContentType($request);
  }

}
