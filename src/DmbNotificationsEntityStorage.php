<?php

namespace Drupal\dismissible_message_bar;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for DMB Notifications entities.
 *
 * This extends the base storage class, adding required special handling for
 * Notifications entity entities.
 *
 * @ingroup dismissible_message_bar
 */
class DmbNotificationsEntityStorage extends SqlContentEntityStorage implements DmbNotificationsEntityStorageInterface {

}
