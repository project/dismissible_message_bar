<?php

namespace Drupal\dismissible_message_bar;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of DMB Notifications entity entities.
 *
 * @ingroup dismissible_message_bar
 */
class DmbNotificationsEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('DMB Notifications entity ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\dismissible_message_bar\Entity\DmbNotificationsEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.dmb_notifications_entity.edit_form',
      ['dmb_notifications_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
