<?php

namespace Drupal\dismissible_message_bar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Class DmbNotificationsEntityController.
 *
 *  Returns responses for Notifications entity routes.
 */
class DmbNotificationsEntityController extends ControllerBase implements ContainerInjectionInterface {

}
