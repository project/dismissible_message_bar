<?php

namespace Drupal\dismissible_message_bar\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of marketo form bundle entities.
 *
 * @ingroup dismissible_message_bar
 *
 * @see \Drupal\dismissible_message_bar\Entity\DmbNotificationType
 */
class DmbNotificationTypeListBuilder extends ConfigEntityListBuilder {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['description'] = [
      'data' => $this->t('Description'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\dismissible_message_bar\Entity\DmbNotificationsEntityInterface $entity */
    $row['label'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];
    $row['description']['data'] = ['#markup' => $entity->getDescription()];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t('No DMB notification types available. @link.', [
      '@link' => Link::fromTextAndUrl($this->t('Add DMB notification type'), Url::fromRoute('dmb_notification_type.add'))->toString(),
    ]);

    return $build;
  }

}
