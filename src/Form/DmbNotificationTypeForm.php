<?php

namespace Drupal\dismissible_message_bar\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DmbNotificationTypeForm.
 *
 * @package Drupal\dismissible_message_bar\Form
 */
class DmbNotificationTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\dismissible_message_bar\Entity\DmbNotificationType $type */
    $form = parent::form($form, $form_state);
    $type = $this->entity;

    $form['label'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#default_value' => $type->label(),
      '#description' => $this->t('The human-readable name of this DMB Notification Type. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => [
          'Drupal\dismissible_message_bar\Entity\DmbNotificationType', 'load',
        ],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this Notification Type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $type->getDescription(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('DMB Notification Type @bundle successfully updated.', [
        '@bundle' => $form_state->getValue('label'),
      ]));
    }
    elseif ($status == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('DMB Notification Type @bundle created successfully.', [
        '@bundle' => $form_state->getValue('label'),
      ]));
    }

    $form_state->setRedirect('entity.dmb_notification_type.collection');
    return $this->entity;
  }

}
