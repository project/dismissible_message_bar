<?php

namespace Drupal\dismissible_message_bar\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for DMB Notifications entity edit forms.
 *
 * @ingroup dismissible_message_bar
 */
class DmbNotificationsEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label DMB Notifications entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label DMB Notifications entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('view.dmb_notifications.dmb_notification_list');
  }

}
