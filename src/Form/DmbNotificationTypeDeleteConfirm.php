<?php

namespace Drupal\dismissible_message_bar\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DmbNotificationTypeDeleteConfirm.
 *
 * @package Drupal\dismissible_message_bar\Form
 */
class DmbNotificationTypeDeleteConfirm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $num_forms = $this->entityTypeManager->getStorage('dmb_notifications_entity')->getQuery()
      ->condition('type', $this->entity->id())
      ->accessCheck(TRUE)
      ->count()
      ->execute();

    if (!empty($num_forms)) {
      $common = ' You can not remove this DMB notification type until you have removed all of the %type DMB notifications.';
      $single = '%type type is used by 1 DMB notification on your site.' . $common;
      $multiple = '%type type is used by @count DMB notifications on your site.' . $common;
      $replace = ['%type' => $this->entity->label()];

      $form['#title'] = $this->getQuestion();
      $form['description'] = [
        '#type' => 'container',
        '#markup' => $this->formatPlural($num_forms, $single, $multiple, $replace),
      ];

      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

}
