<?php

namespace Drupal\dismissible_message_bar\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting DMB Notifications entity entities.
 *
 * @ingroup dismissible_message_bar
 */
class DmbNotificationsEntityDeleteForm extends ContentEntityDeleteForm {


}
